export default {
  isLogged (state) {
    return state.isLogged
  },
  isLoading (state) {
    return state.loading
  },
  getUser (state) {
    return state.userLogged
  },
  getProfileStatus (state) {
    return state.profileClicked
  },
  getMonths (state) {
    return state.months
  },
  getCaseResults (state) {
    return state.caseResults
  },
  getDepartments (state) {
    return state.departments
  },
  getCourts (state) {
    return state.courts
  },
  getUserTypes (state) {
    return state.userTypes
  },
  getStatuses (state) {
    return state.statuses
  },
  getCurrentPage (state) {
    return state.currentPage
  }
 }
