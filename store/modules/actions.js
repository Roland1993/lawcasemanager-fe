import makeRequestTo from '../../src/services/makeRequestTo'

export default {
  logout({state}) {
    state.token = null
    state.isLogged = null
    state.userLogged = null
    localStorage.removeItem('vuex')
  }
}
