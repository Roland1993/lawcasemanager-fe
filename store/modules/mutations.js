export default {
  changeProfileStatus (state, payload ) {
    state.profileClicked = payload
  },
  changeIsLoading (state, payload) {
    state.loading = payload
  },
  setUserLogged (state, payload) {
    state.userLogged = payload
  },
  changeIsLogged (state, payload) {
    state.isLogged = payload
  },
  setToken (state, payload) {
    state.token = payload
  },
  changeCurrentPage (state, payload) {
    state.currentPage = payload
  }
}
