export default {
  currentPage: '',
  isLogged: false,
  token: null,
  profileClicked: false,
  userLogged:null,
  loading: false,
  months: [
    { id: 1, monthText: 'Janar' },
    { id: 2, monthText: 'Shkurt' },
    { id: 3, monthText: 'Mars' },
    { id: 4, monthText: 'Prill' },
    { id: 5, monthText: 'Maj' },
    { id: 6, monthText: 'Qershor' },
    { id: 7, monthText: 'Korrik' },
    { id: 8, monthText: 'Gusht' },
    { id: 9, monthText: 'Shtator' },
    { id: 10, monthText: 'Tetor' },
    { id: 11, monthText: 'Nëntor' },
    { id: 12, monthText: 'Dhjetor' },
  ],
  caseResults: [
    { id: 1, caseResult: 'Fituar' },
    { id: 2, caseResult: 'Humbur' },
    { id: 3, caseResult: 'Fituar pjesërisht' },
    { id: 4, caseResult: 'Pushuar' },
    { id: 5, caseResult: 'Transferuar' },
    { id: 6, caseResult: 'Jo e përfaqësuar' },
    { id: 7, caseResult: 'aktive' },
  ],
  departments: [
    { id: 1, departmentText: 'Zyra e përfaqësimit në gjykatat kombëtare' },
    { id: 2, departmentText: 'Zyra e këshillimit dhe koordinimit ndërministror' },
    { id: 3, departmentText: 'Zyra e përfaqësimit në gjykatat e huaja dhe arbitrazhi' },
    { id: 4, departmentText: 'Zyra e inspektimit' },
  ],
  courts: [
    { id: 1, courtText: 'Shkalla e parë' },
    { id: 2, courtText: 'Shkalla e dytë' },
    { id: 3, courtText: 'Shkalla e tretë' },
    { id: 4, courtText: 'Shkalla e katërt' },
  ],
  userTypes: [
    { id: 1, userTypeText: 'Përdorues Bazik' },
    { id: 2, userTypeText: 'Përdorues Normal' },
    { id: 3, userTypeText: 'Super Përdorues' },
    { id: 4, userTypeText: 'Admin' },
  ],
  statuses: [
    { id: 1, statusText: 'I Punësuar' },
    { id: 2, statusText: 'Jo i punësuar' }
  ]
}
