import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import createPersistedState from 'vuex-persistedstate'
import State from './modules/state'
import Getters from './modules/getters'
import Mutations from './modules/mutations'
import Actions from './modules/actions'

Vue.use(Vuex);

export const store  = new Vuex.Store({
  state: State,
  getters: Getters,
  mutations: Mutations,
  actions: Actions,
  plugins: [createPersistedState({
    reducer: state => ({
      token: state.token,
      userLogged: state.userLogged,
      isLogged: state.isLogged
    })
  })]
  // strict: process.env.NODE_ENV !== 'production' //turn off strict mode in production
});
