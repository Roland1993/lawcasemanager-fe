import {store} from '../store/store'

export class Auth {

  static USERS_LIST_PERMISSION = 'list-users';
  static USERS_VIEW_PERMISSION = 'view-users';
  static USERS_ADD_PERMISSION = 'add-users';
  static USERS_EDIT_PERMISSION = 'edit-users';
  static USERS_DELETE_PERMISSION = 'delete-users';
  static USERS_EDIT_THEIR_FULL_NAME= 'edit-full-name';
  static CASE_LIST_PERMISSION = 'list-case'
  static IS_SUPER_USER = 'is-super-user'

  static hasPermission(permission) {
    // if (!store.getters.getUser)
    //   return
    switch (permission) {
      case Auth.USERS_LIST_PERMISSION:
        return (store.getters.getUser.userType !== 1);
        break;
      case Auth.USERS_DELETE_PERMISSION:
        return (store.getters.getUser.userType === 3);
        break;
      case Auth.USERS_EDIT_THEIR_FULL_NAME:
        return (store.getters.getUser.userType === 3)
        break
      case Auth.CASE_LIST_PERMISSION:
        return (store.getters.getUser.userType !== 1)
        break
      case Auth.IS_SUPER_USER:
        return (store.getters.getUser.userType === 3)
        break
      default:
        return false;
    }
  }

  static isUserLogged() {
    return !!store.state.token
  }


}

export const hasPermission = (permission) => {
  return Auth.hasPermission(permission);
};
