import { store } from '../../store/store'
import request from '../axios-intances/request'

export default {

/* CASE ROUTES */

  addNewCase (payload)  {
    return request.post('/add/new/case', payload)
  },
  updateCase (payload) {
    return request.put('/update/case', payload)
  },
  deleteCase (payload) {
    return request.delete('/delete/case', { params: payload })
  },
  checkCaseNumber (payload) {
    return request.post('/check/case/number', payload)
  },
  getCaseNumbers (payload) {
    return request.post('/caseNumbers', payload)
  },

/*CASE ROUTES*/

/* ============================= */

/*SESSION CASE ROUTES*/

  updateSessionCase (payload) {
    return request.put('/update/session/case', payload)
  },
  deleteSessionCase (payload) {
    return request.delete('/delete/session/case', { params: payload })
  },
  addSessionCase (payload) {
    return request.post('/add/new/session/case', payload)
  },

/*SESSION CASE ROUTES*/

/* ============================= */

/* USER ROUTES */

  addNewUser (payload) {
    return request.post('/add/new/user', payload)
  },
  updateUser (payload) {
    return request.put('/update/user', payload)
  },
  deleteUser (payload) {
    return request.delete('/deleteUser', { params: payload })
  },
  changePassword (payload) {
    return request.put('/change/password', payload)
  },
  checkEmail (url, payload) {
    return request.post(url, payload)
  },
  getLawyers () {
    return request.get('/getLawyers')
  },


/* USER ROUTES */

/* ============================= */

/* CASERESULT ROUTES */

  getCaseResults() {
    return request.get('/caseResults')
  },
  updateCaseResult (payload) {
    return request.put('/update/caseResult', payload)
  },
  deteleCaseResult (payload) {
    return request.delete('/delete/caseResult', { params: payload })
  },

/* CASERESULT ROUTES */

/* ============================= */

/* OTHER ROUTES */

  login (data) {
    return request.post('/auth/login', data)
  },
  fillTable (url, payload) {
    return request.post(url, payload)
  }

/* OTHER ROUTES */



























}
