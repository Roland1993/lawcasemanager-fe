import { store } from '../../store/store'

export default {
  userType () {
    return store.getters.getUser.userType
  },
  setCurrentPage (payload) {
    store.commit('changeCurrentPage', payload)
  },
  isSuperUser() {
    return this.userType() === 3
  },
  isBasicUser() {
    return this.userType() === 1
  },
  isNormalUser() {
    return this.userType() === 2
  },
  isAdmin() {
    return this.userType() === 4
  },
  managePermissions (currentPage, action) {
    switch (currentPage) {
      case 'case':
        return this.cases(action)
      case 'sessionCase':
        return this.sessionCases(action)
      case 'user':
        return this.users(action)
      default:
        return false
    }
  },
  cases (action) {
    switch (action) {
      case 'add':
      case 'edit':
      case 'delete':
        return  this.isAdmin() || this.isSuperUser()
      case 'view':
        return true
      default:
        return false
    }

  },
  sessionCases (action) {
    switch (action) {
      case 'edit':
      case 'delete':
      case 'add':
        return !this.isNormalUser()
      case 'view':
        return true
      default:
        return false
    }
  },
  users (action) {
    switch (action) {
      case 'add':
      case 'edit':
      case 'delete':
      case 'view':
        return this.isAdmin()
      default:
        return false
    }

  }

}
