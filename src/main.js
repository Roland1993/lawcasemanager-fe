// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import { store } from '../store/store'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import Vuelidate from 'vuelidate'
import Toasted from 'vue-toasted'
import Auth from './services/Auth'

export const eventBus = new Vue()

Vue.use(Vuetify, { theme: {
  primary: '#ee44aa',
  secondary: '#424242',
  accent: '#82B1FF',
  error: '#FF5252',
  info: '#2196F3',
  success: '#4CAF50',
  warning: '#FFC107'
}})

Vue.use(Auth)

Vue.directive('gradient', (el) => {
  el.style.background = 'linear-gradient(to right, #0f0c29, #302b63, #24243e)'
})

Vue.config.productionTip = false

Vue.use(Vuelidate)
Vue.use(Toasted)




router.beforeEach((to, from, next) => {
  if (to.name === 'Login') {
    if (Vue.auth.isAuthenticated()) {
      next({name: 'notFound'})
    }else {
      next()
    }
  }else if (to.name === 'notFound') {
      next()
  }else {
    if (Vue.auth.isAuthenticated()) {
      next()
    } else {
      next({name: 'notFound'})
    }
  }

})


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
