import { required } from 'vuelidate/lib/validators'
import makeRequestTo from '@/services/makeRequestTo'
import _ from 'lodash'

export const validations = {
  validations: {
    caseNumber: {
      required,
    },
    description: {
      required,
    }
  },
  methods: {
    validate (searchedText) {
      if (searchedText === '') return false
      if (searchedText === this.editedRow.caseNumber) return false
      this.checkCaseNumber(searchedText)
    },
    checkCaseNumber: _.debounce(function (caseNumber) {
      if (caseNumber === this.editedRow.caseNumber || caseNumber === '') {
        this.isCaseNumberValid = true
      }else {
        const payload = {
          caseNumber
        }
        makeRequestTo.checkCaseNumber(payload)
          .then(response => {
            this.isCaseNumberValid = response.data
          })
      }
    },500)
  }
}
