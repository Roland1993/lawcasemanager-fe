import { validations } from "./edit-session-case-mixins/validations";
import { snackbar } from "../../mixins/snackbar";
import makeRequestTo from "../../services/makeRequestTo";

export default {
  mixins: [validations, snackbar],
  props: {
    editedRow: {
      required: true
    },
    dialog: {
      default: true
    }
  },
  data() {
    return {
      loading: false,
      timeout: null,
      caseNumber: this.editedRow.caseNumber,
      description: this.editedRow.description,
      isCaseNumberValid: true
    }
  },
  created () {
    this.caseNumber = this.editedRow.caseNumber
    this.description = this.editedRow.description
  },
  methods: {
    saveEdited () {
      if (!this.$v.$invalid && this.isCaseNumberValid === true) {
        if (this.somethingChanged()) {
          const updatedSessioCase = {
            id: this.editedRow.id,
            caseNumber: this.caseNumber,
            description: this.description
          }

          this.loading = true
          makeRequestTo.updateSessionCase(updatedSessioCase)
            .then(response => {
              this.loading = false
              this.openSuccessSnackbar(response.data.message)
              this.$emit('tableUpdated')
              this.closeDialog()
            })
        }else {
          this.openErrorSnackbar('Ju nuk ndryshuat asgjë')
        }
      }else {
        this.openErrorSnackbar('Plotësoni saktë të dhënat')
      }
    },
    somethingChanged () {
      if (this.caseNumber === this.editedRow.caseNumber && this.description === this.editedRow.description){
        return false
      }
      return true
    },
    closeDialog () {
      this.$emit('update:dialog', false)
    }
  },
  name: 'EditSessionCase'
}
