import makeRequestTo from "../../../services/makeRequestTo";
import { eventBus } from "../../../main";

export const methods = {
  methods: {
    manageQueue() {
      this.loading = true
      this.monthItems = this.getMonths
      this.courtItems = this.getCourts

        makeRequestTo.getLawyers()
          .then(response => {
            this.loading = false
            this.lawyerItems = response.data.lawyers
          })
    },
    closeDialog () {
      this.$emit('update:dialog', false)
    },
    async save () {
      this.$v.$touch()
      if (this.$v.$invalid) {
        this.openSuccessSnackbar('Plotësoni saktë të dhënat')
      }else {
        const $case = {
          nrakti: this.nrakti,
          plaintiff: this.plaintiff,
          defendent: this.defendent,
          thirdParties: this.thirdParties,
          object: this.object,
          claimValue: this.claimValue,
          judge: this.judge,
          court: this.court,
          month: this.month,
          lawyerId: this.lawyer,
          archived: this.archived
        }

        this.loading = true
        makeRequestTo.addNewCase($case)
          .then(response => {
            this.loading = false
            this.openSuccessSnackbar(response.data.message)
            if (this.$route.name === 'Homepage') {
              eventBus.$emit('caseAdded')
            }
            this.$emit('update:dialog', false)
          })
      }
    }
  }
}
