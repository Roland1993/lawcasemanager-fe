import { required, numeric } from 'vuelidate/lib/validators'

export const validations = {
  validations: {
    nrakti: { required },
    plaintiff: { required },
    defendent: { required },
    thirdParties: { required },
    object: { required },
    claimValue: { required, numeric },
    judge: { required },
    court: { required },
    month: { required },
    lawyer: { required },
    archived: { required }
  }
}
