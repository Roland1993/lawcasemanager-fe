import { validations } from "./add-case-mixins/validations";
import { methods } from "./add-case-mixins/methods";
import { snackbar } from "../../mixins/snackbar";
import { mapGetters } from 'vuex'

export default {
  mixins: [validations, methods, snackbar],
  props: {
    dialog: {
      default: false
    }
  },
  data () {
    return {
      loading: false,
      nrakti: null,
      plaintiff: null,
      defendent: null,
      thirdParties: null,
      object: null,
      claimValue: null,
      judge: null,
      court: null,
      month: null,
      lawyer: null,
      archived: 'Po',
      monthItems: [],
      lawyerItems: [],
      courtItems: [],
    }
  },
  created () {
    this.manageQueue()
  },
  computed: {
    ...mapGetters([
      'getAddCaseStatus',
      'getCourts',
      'getMonths'
    ]),
    isMobileScreen() {
      if(navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)) {
        return true
      }
      return false
    }
  },

}
