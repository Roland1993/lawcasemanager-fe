import makeRequestTo from '@/services/makeRequestTo'
import { required } from 'vuelidate/lib/validators'
import { snackbar } from "../../mixins/snackbar";

export default {
  mixins: [snackbar],
  props: {
    dialog: {
      default: false,
    }
  },
  validations: {
    caseNumber: { required },
    description: { required }
  },
  data() {
    return {
      loading: false,
      caseNumber: null,
      description: null,
      caseNumberItems: [],
      selectSearch: null,
      selectLoading: false
    }
  },
  watch: {
    selectSearch (val) {
      val && this.getCaseNumbers(val)
    }
  },
  methods: {
    getCaseNumbers (searchedText) {
      this.selectLoading = true
      const payload = {
        caseNumber: searchedText
      }
      makeRequestTo.getCaseNumbers(payload)
        .then(response => {
          this.selectLoading = false
          this.caseNumberItems = response.data.caseNumbers
        })
    },
    close () {
      this.$emit('update:dialog', false)
    },
    save () {
      this.$v.$touch()
      if (!this.$v.$invalid && this.caseNumber) {
        const sessionCaseData = {
          caseNumber: this.caseNumber,
          description: this.description
        }

        this.loading = true
        makeRequestTo.addSessionCase(sessionCaseData)
          .then(response => {
            this.loading = false
            this.openSuccessSnackbar(response.data.message)
            this.$emit('tableUpdated')
            this.close()
          })
      }else {
        this.openErrorSnackbar('Plotësoni saktë të dhënat')
      }
    }
  }
}
