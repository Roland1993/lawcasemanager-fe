import { methods } from "./profile-mixins/methods";

export default {
  mixins: [methods],
  data () {
    return {
      name: null,
      lname: null,
      email: null,
      password: null,
      department: null,
      userType: null,
      status: null,
    }
  },
  mounted () {
    if (this.getUser) {
      this.name = this.getUser.nameSurname.substr(0, this.getUser.nameSurname.indexOf(' '))
      this.lname = this.getUser.nameSurname.substr(this.getUser.nameSurname.indexOf(' ') + 1)
      this.email = this.getUser.email
      this.department = this.getUser.departmentText
      this.userType = this.getUser.userTypeText
      this.status = this.getUser.statusText
    }
  },
  computed: {
    getUser() {
      return this.$store.getters.getUser
    },
    getProfileStatus() {
      return this.$store.getters.getProfileStatus
    },
    isMobileScreen() {
      if(navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)) {
        return true
      }
      return false
    }
  },
  name: 'Profile'
}
