import { eventBus } from "../../../main";

export const methods = {
  methods: {
    closeDialog () {
      this.$store.commit('changeProfileStatus',false)
    },
    async changePassword () {
      await this.openChangePasswordModal()
      eventBus.$emit('changePassword', {
        open: true,
        userID: null
      })
    },
    openChangePasswordModal () {
      return new Promise((resolve, reject) => {
        this.$store.commit('changeProfileStatus',false)
        setTimeout(() => {
          resolve(true)
        },150)
      })
    }
  }
}
