import { validations } from "./add-user-mixins/validations";
import { methods } from "./add-user-mixins/methods";
import { snackbar } from "../../mixins/snackbar";
import { mapGetters } from 'vuex'

export default {
  mixins: [validations, methods, snackbar],
  props: {
    modal: {
      default: false
    }
  },
  created () {
    this.manageQueue()
  },
  data() {
    return {
      timeout: null,
      loading: false,
      name: '',
      lastName: '',
      email: '',
      password: '',
      repeatPassword: '',
      department: '',
      userType: '',
      status: '',
      departmentItems:[],
      userTypeItems: [],
      statusItems: [],
      saveButtonAbleToClick: true,
    }
  },
  computed: {
    ...mapGetters([
      'getDepartments',
      'getUserTypes',
      'getStatuses'
    ])
  }
}
