import makeRequestTo from '../../../services/makeRequestTo'
import { required, sameAs, email } from 'vuelidate/lib/validators'

export const validations = {
  validations: {
    name: { required },
    lastName: { required },
    email: {
      required,
      email,
      async isUnique (email) {
        if (email === '') return true
        if (!this.$v.email.email) return true

        const payload = {
          email
        }

        await this.waitBeforeValidate()
        return await this.makeRequest(payload)

      }
    },
    password: { required },
    repeatPassword: { samePassword: sameAs('password') },
    department: { required },
    userType: { required },
    status: { required }
  },
  methods: {
    waitBeforeValidate () {
      if (this.timeout) {
        clearTimeout(this.timeout)
      }
      return new Promise(resolve => this.timeout = setTimeout(resolve, 1000))
    },
    makeRequest (payload) {
      return makeRequestTo.checkEmail('/checkEmail', payload)
        .then(response => {
          if (response.data === true) {
            return false
          }else {
            return true
          }
        })

    },
  }
}
