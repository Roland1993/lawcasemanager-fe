import axios from 'axios'
import makeRequestTo from '@/services/makeRequestTo'

export const methods = {
  methods: {
    async manageQueue () {
      this.departmentItems = this.getDepartments
      this.userTypeItems = this.getUserTypes
      this.statusItems = this.getStatuses
    },
    close () {
      this.$emit('update:modal', false)
    },
    save () {
      this.$v.$touch()
      if (this.$v.$invalid) {
        this.openErrorSnackbar('Plotësoni saktë të dhënat')
      }else {
        const userData = {
          nameSurname: this.name + ' ' + this.lastName,
          email: this.email,
          password: this.password,
          department: this.department,
          userType: this.userType,
          status: this.status
        }

        this.loading = true
        makeRequestTo.addNewUser(userData)
          .then(response => {
            this.loading = false
            this.openSuccessSnackbar(response.data.message)
            this.$emit('tableUpdated')
            this.close()
          })
      }
    }
  }
}
