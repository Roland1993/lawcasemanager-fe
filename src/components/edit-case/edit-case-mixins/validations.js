import { required } from 'vuelidate/lib/validators'

export const validations = {
  validations: {
    claimValue: { required },
    defendent: { required },
    nrakti: { required },
    object: { required },
    plaintiff: { required },
    thirdParties: { required },
  }
}
