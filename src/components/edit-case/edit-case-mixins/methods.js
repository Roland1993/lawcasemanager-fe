import axios from 'axios'
import makeRequestTo from '@/services/makeRequestTo'

export const methods = {
  methods: {
    async manageQueue() {
      this.loading = true
      await this.makeRequests()
      this.fillModalFields(this.tableRow)
      this.loading = false
    },
    makeRequests() {
      this.caseResultItems = this.getCaseResults
      this.courtItems = this.getCourts

      return makeRequestTo.getLawyers()
        .then(response => {
          this.lawyerItems = response.data.lawyers
        })
    },
    fillModalFields(tableRow) {
      this.archived = tableRow.archived
      this.caseResult = tableRow.caseResultID
      this.claimValue = tableRow.claimValue
      this.court = Number(tableRow.court)
      this.defendent = tableRow.defendent
      this.lawyer = tableRow.lawyerId
      this.nrakti = tableRow.nrakti
      this.object = tableRow.object
      this.plaintiff = tableRow.plaintiff
      this.thirdParties = tableRow.thirdParties
      this.courtDecision = tableRow.courtDecision
      this.decisionNumber = tableRow.decisionNumber
    },
    closeDialog() {
      this.$emit('update:dialog', false)
    },
    async saveEdited() {
      if (this.validateFields()) {
        const $case = this.returnCaseChangedFields()
        const $caseResult = this.returnCaseResultChangedFields()

        if (!$case && !$caseResult) {
          this.openErrorSnackbar('Ju nuk ndryshuar asgjë')
          return
        }

        if ($case) {
          await makeRequestTo.updateCase($case)
        }

        if ($caseResult) {
          await makeRequestTo.updateCaseResult($caseResult)
        }

        this.openSuccessSnackbar('Cëshjta u ndryshua me sukses')
        this.$emit('tableUpdated')
        this.closeDialog()
      } else {
        this.openErrorSnackbar('Plotësoni saktë fushat')
      }
    },
    validateFields() {
      if (this.$v.$invalid) {
        return false //fields have error
      }
      return this.isSelectFieldsValid()
    },
    isSelectFieldsValid() {
      /* In case the user wants to change the select items thought inspect,we making sure that this will not affect security of application*/
      let validArchived, validLawyer, validCaseResult, validCourt, error
      validArchived = validLawyer = error = validCaseResult, validCourt = false

      if (this.existItemInArrayOfItems(this.archivedItems, this.archived, 'item.value')) validArchived = true
      if (this.existItemInArrayOfItems(this.lawyerItems, this.lawyer, 'item.id')) validLawyer = true
      if (this.existItemInArrayOfItems(this.caseResultItems, this.caseResult, 'item.id')) validCaseResult = true
      if (this.existItemInArrayOfItems(this.courtItems, this.court, 'item.id')) validCourt = true

      if (validArchived && validLawyer && validCaseResult && validCourt) return true

      return false
    },
    existItemInArrayOfItems(items, property, itemsValue) {
      for (let item of items) {
        if (property == eval(itemsValue)) {
          return true
        }
      }

      return false
    },
    returnCaseChangedFields() {
      let obj = {}

      if (this.archived !== this.tableRow.archived) obj.archived = this.archived
      if (this.claimValue !== this.tableRow.claimValue) obj.claimValue = this.claimValue
      if (this.court != this.tableRow.court) obj.court = this.court
      if (this.defendent !== this.tableRow.defendent) obj.defendent = this.defendent
      if (this.lawyer !== this.tableRow.lawyerId) obj.lawyerId = this.lawyer
      if (this.nrakti !== this.tableRow.nrakti) obj.nrakti = this.nrakti
      if (this.object !== this.tableRow.object) obj.object = this.object
      if (this.plaintiff !== this.tableRow.plaintiff) obj.plaintiff = this.plaintiff
      if (this.thirdParties !== this.tableRow.thirdParties) obj.thirdParties = this.thirdParties

      if (Object.keys(obj).length !== 0) {
        obj.id = this.tableRow.id
        return obj
      }

      return false
    },
    returnCaseResultChangedFields() {
      let obj = {}

      if (this.caseResult !== this.tableRow.caseResultID) obj.courtResult = this.caseResult
      if (this.courtDecision !== this.tableRow.courtDecision) obj.courtDecision = this.courtDecision
      if (this.decisionNumber !== this.tableRow.decisionNumber) obj.decisionNumber = this.decisionNumber

      if (Object.keys(obj).length !== 0) {
        obj.id = this.tableRow.id
        return obj
      }

      return false

    },
  }
}
