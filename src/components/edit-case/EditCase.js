import { methods } from "./edit-case-mixins/methods"
import { validations } from "./edit-case-mixins/validations"
import { snackbar } from "../../mixins/snackbar";
import { mapGetters } from "vuex"

export default {
  mixins: [methods,validations, snackbar],
  props: {
    dialog: {
      default: false
    },
    tableRow: {
      required: true
    }
  },
  created () {
    this.manageQueue()
  },
  data() {
    return {
      saveButtonAbleToClick: false,
      loading: false,
      archived: null,
      caseResult: null,
      claimValue: null,
      court: null,
      defendent: null,
      lawyer: null,
      nrakti: null,
      object: null,
      plaintiff: null,
      thirdParties: null,
      courtDecision: null,
      decisionNumber: null,
      archivedItems: [
        { text: 'Arkivuar', value: 'Po' },
        { text: 'Jo e arkivuar', value: 'Jo' }
      ],
      caseResultItems: [],
      courtItems: [],
      lawyerItems: []
    }
  },
  computed: {
    ...mapGetters([
      'getCaseResults',
      'getCourts'
    ])
  },
  name: 'EditCase'

}
