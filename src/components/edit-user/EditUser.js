import { validations } from "./edit-user-mixins/validations";
import { methods } from "./edit-user-mixins/methods";
import checkUser from "../../services/checkUser";
import { snackbar } from "../../mixins/snackbar";
import { mapGetters } from 'vuex'


export default {
  mixins: [validations, methods, snackbar],
  props: {
    editedRow: {
      required: true
    },
    dialog: {
      default: false
    }
  },
  data: () => ({
    loading: false,
    checkUser: checkUser,
    timeout: null,
    name: null,
    lastName: null,
    email: null,
    department: null,
    userType: null,
    status: null,
    departmentItems: [],
    userTypeItems: [],
    statusItems: []
  }),
  created () {
    this.manageQueue()
  },
  computed: {
    ...mapGetters([
      'getUser',
      'getDepartments',
      'getUserTypes',
      'getStatuses'
    ]),
  },
  name: 'EditUser'
}
