import makeRequestTo from '@/services/makeRequestTo'
import axios from 'axios'
import { eventBus } from "@/main";

export const methods = {
  methods: {
    manageQueue () {
      this.makeRequests()
      this.fillModalFields(this.editedRow)
    },
    makeRequests() {
      this.departmentItems = this.getDepartments
      this.userTypeItems = this.getUserTypes
      this.statusItems = this.getStatuses
    },
    fillModalFields (row) {
      this.name = row.nameSurname.substr(0, this.editedRow.nameSurname.indexOf(' '))
      this.lastName = row.nameSurname.substr(this.editedRow.nameSurname.indexOf(' ') + 1)
      this.email = row.email
      this.department = Number(row.department)
      this.userType = row.userType
      this.status = Number(row.status)
    },
    closeDialog () {
      this.$emit('update:dialog')
    },
    saveEdited () {
      if (this.validateFields()) {
        const $user = this.returnUserChangedFields()

        if ($user) {
          this.loading = true
          makeRequestTo.updateUser($user)
            .then(response => {
              this.loading = false
              this.openSuccessSnackbar('Të dhënat e përdoruesit u ndryshuan me sukses')
              this.$emit('tableUpdated')
              this.closeDialog()
              return
            })
        }else {
          this.openErrorSnackbar('Ju nuk ndryshuat asgjë')
        }

      }else {
        this.openErrorSnackbar('Plotësoni saktë fushat')
      }

    },
    validateFields () {
      if (this.$v.$invalid) {
        return false
      }else {
        return this.isSelectFieldsValid()
      }
    },
    isSelectFieldsValid () {
      let validDepartment, validUserType, validStatus, error
      validDepartment = validUserType = validStatus = error = false

      if (this.existItemInArrayOfItems(this.departmentItems, this.department, 'item.id')) validDepartment = true
      if (this.existItemInArrayOfItems(this.userTypeItems, this.userType, 'item.id')) validUserType = true
      if (this.existItemInArrayOfItems(this.statusItems, this.status, 'item.id')) validStatus = true

      if (validDepartment && validUserType && validStatus) return true

      return false
    },
    existItemInArrayOfItems(items, property, itemsValue) {
      for (let item of items) {
        if (property == eval(itemsValue)) {
          return true
        }
      }

      return false
    },
    returnUserChangedFields () {
      let obj = {}
      let name = this.editedRow.nameSurname.substr(0, this.editedRow.nameSurname.indexOf(' '))
      let lastName = this.editedRow.nameSurname.substr(this.editedRow.nameSurname.indexOf(' ') + 1)

      if (this.name !== name || this.lastName !== lastName) obj.nameSurname = this.name + ' ' + this.lastName
      if (this.email !== this.editedRow.email) obj.email = this.email
      if (this.department != this.editedRow.department) obj.department = this.department
      if (this.userType !== this.editedRow.userType) obj.userType = this.userType
      if (this.status != this.editedRow.status) obj.status = this.status

      if (Object.keys(obj).length !== 0) {
        obj.id = this.editedRow.id
        return obj
      }

      return false

    },
    async changePassword () {
      const id = this.editedRow.id
      await this.openChangePasswordModal()
      eventBus.$emit('changePassword', {
        open: true,
        userID: id
      })
    },
    openChangePasswordModal () {
      return new Promise((resolve, reject) => {
        this.closeDialog()

        setTimeout(() => {
          resolve(true)
        },150)
      })
    }
  }
}
