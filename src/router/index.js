import Vue from 'vue'
import checkUser from '../services/checkUser'
import Router from 'vue-router'
import { store } from "../../store/store";
import Homepage from '@/pages/home/Home.vue'
import Users from '@/pages/users/Users.vue'
import SessionCase from '@/pages/session-cases/SessionCases.vue'
import NotFound from '@/pages/not-found/NotFound.vue'
import Login from '@/pages/login/Login.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Homepage',
      component: Homepage,
      beforeEnter (to, from, next) {
        checkUser.setCurrentPage('Kryefaqja')
        next()
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/users',
      name: 'Users',
      component: Users,
      beforeEnter(to, from, next) {
        if (!checkUser.isAdmin()) {
          next({name: 'notFound'})
        }
        checkUser.setCurrentPage('Përdoruesit')
        next()
      }
    },
    {
      path: '/seancatDitore',
      name: 'SessionCase',
      component: SessionCase,
      beforeEnter(to, from, next) {
        checkUser.setCurrentPage('Seancat Ditore')
        next()
      }
    },
    {
      path: '/404',
      alias: '*',
      name: 'notFound',
      component: NotFound,
      beforeEnter(to, from, next) {
        checkUser.setCurrentPage('Kjo faqe nuk ekziston')
        next()
      }
    }
  ],
  mode: 'history',
  base: '/avokatura'
})
