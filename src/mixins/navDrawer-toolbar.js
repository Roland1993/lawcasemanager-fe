import checkUser from '../services/checkUser'

export const navDrawerToolbar = {
  computed: {
    userPermissions() {
      let pagesToView = [
        { icon: 'today', text: 'Seancat Ditore', link: '/seancatDitore' },
        { icon: 'business_center', text: 'Çështjet', link: '/' },
        { icon: 'account_circle', text: 'Profili', link: '', action: 'profile'},
        { icon: 'exit_to_app', text: 'Dil', link: '', action: 'logout'}
      ]

      if (checkUser.isAdmin()) {
        const pages = { icon: 'group', text: 'Përdoruesit', link: '/users' }
        pagesToView.splice(3, 0, pages)
      }

      return pagesToView

    }
  },
  methods: {
    linkClicked (link) {
      if(link.trim() === '')
        return

      if(link.trim() === 'profile') {
        this.profileSettings()
      }else if (link.trim() === 'logout') {
        this.logout()
      }
    },
    profileSettings () {
      const openProfile = !this.getProfileStatus
      this.$emit('onDrawerClose',false)
      this.$store.commit( 'changeProfileStatus', openProfile )
    },
    logout() {
      this.$store.dispatch('logout')
      this.$router.push({name: 'Login'})
    }
  }
}
