import { eventBus } from "../main";

export const snackbar = {
  methods: {
    openSuccessSnackbar (text) {
      eventBus.$emit('snackBar', {
        text: text,
        snackbarColor: 'deep-purple',
      })
    },
    openErrorSnackbar (text) {
      eventBus.$emit('snackBar', {
        text: text,
        snackbarColor: 'deep-purple',
      })
    }
  }
}
