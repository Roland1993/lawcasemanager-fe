import axios from 'axios'
import { store } from "../../store/store"
import { eventBus } from "../main";

const request = axios.create({
  baseURL: 'http://localhost:8000'
})

request.interceptors.request.use(request => {
  const token = store.state.token
  if (token) {
    request.headers.Authorization = 'Bearer ' + token
  }
  return request

}, error => {
  return Promise.reject(error)
})

request.interceptors.response.use(response => {
  return response
}, error => {

  switch (error.response.status) {
    case 401:
      eventBus.$emit('logout')
      break
    default:
      eventBus.$emit('snackBar', {
        text: error.response.data.message,
        snackbarColor: 'red darken-4'
      })
  }
  return Promise.reject(error )
})

export default request
