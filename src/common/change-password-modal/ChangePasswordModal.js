import { required,sameAs } from 'vuelidate/lib/validators'
import { mapGetters } from 'vuex'
import checkUser from '../../services/checkUser'
import makeRequestTo from '@/services/makeRequestTo'
import { snackbar } from "../../mixins/snackbar";
import { eventBus } from "../../main";

export default {
  mixins: [snackbar],
  props: {
    modal: {
      default: false
    },
    userID: {
      default: false
    }
  },
  validations: {
    oldPassword: {
      required
    },
    newPassword: {
      required
    },
    repeatPassword: {
      sameAs: sameAs('newPassword')
    }
  },
  data() {
    return {
      checkUser: checkUser,
      oldPassword: '',
      newPassword: '',
      repeatPassword: '',
    }
  },
  methods: {
    close () {
      this.$emit('update:modal')
    },
    save() {
      this.$v.$touch()
      if (this.$v.$invalid) {
        this.openErrorSnackbar('Plotësoni saktë të dhënat')
      }else {
        const payload = {
          oldPassword: this.oldPassword,
          newPassword: this.newPassword
        }

        if (this.userID) {
          payload.userID = this.userID
        }

        makeRequestTo.changePassword(payload)
          .then(response => {
            this.openSuccessSnackbar(response.data.message)
            this.close()
          })
      }
    }
  },
  name: 'ChangePassword'
}
