import { navDrawerToolbar } from "../../mixins/navDrawer-toolbar"
import { mapGetters } from 'vuex'

export default {
  mixins: [navDrawerToolbar],
  computed: {
    ...mapGetters([
      'getProfileStatus',
      'getCurrentPage'
    ]),
  },
  methods: {
    drawerClicked () {
      this.$emit('onHamburgerClicked',true)
    },
  }
}
