import { navDrawerToolbar } from "../../mixins/navDrawer-toolbar";

export default {
  mixins: [navDrawerToolbar],
  props: ['drawerPassed'],
  data () {
    return {
      drawer: null,
      items: [
        { title: 'Home', icon: 'dashboard' },
        { title: 'About', icon: 'question_answer' }
      ]
    }
  },
  computed: {
    getUser () {
      return this.$store.getters.getUser
    },
    nameSurname () {
      if(this.getUser) {
        return this.getUser.nameSurname
      }
      return null
    }
  },
  watch: {
    drawerPassed(value) {
      this.drawer = value
    },
    drawer(value) {
      if(value == false)
        this.$emit('onDrawerClose',false)
    }
  }
}
