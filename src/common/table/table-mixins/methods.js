import makeRequestTo from '../../../services/makeRequestTo'

export const methods = {
  methods: {
    async initTable () {
      const url = '/' + this.tableContent
      const payload = {
        page: this.pagination.page,
        rowsPerPage: this.pagination.rowsPerPage,
        sortBy: this.pagination.sortBy,
        descending: this.pagination.descending,
      }


      if (this.filterTable && this.tableBelongsTo('case')) {
        payload.filterTable = this.tableFilters
      }

      if (this.tableBelongsTo('case')) {
        payload.columns = this.headersSelected
      }

      if (this.pagination.search) {
        if (!this.hasSearch) {
          payload.page = 1
          this.pagination.page = 1
          this.hasSearch = true
        }
        payload.search = this.pagination.search
        await this.waitBeforeSearch()
      }

      if (this.hasSearch && !this.pagination.search.trim()) {
        payload.page = 1
        this.pagination.page = 1
      }

      this.loading = true
      makeRequestTo.fillTable(url, payload)
        .then(response => {
          if (this.tableUpdated) { this.$emit('tableFinishUpdate') }
          this.loading = false
          this.items = eval(this.dbResponse)
          this.totalItems = response.data.totalPages
        })
        .catch(e => {
          this.$emit('tableFinishUpdate')
          this.loading = false
        })

    },
    waitBeforeSearch () {
      if (this.searchTimeout) {
        clearTimeout(this.searchTimeout)
      }
      return new Promise(resolve => this.searchTimeout = setTimeout(resolve, 400))
    },
    tableBelongsTo(page) {
      return this.currentPage === page
    },
    editItem(row) {
      let obj = this.items.find( item => item.id === row.id)
      this.$emit('onEditRow', obj)
    },
    fillItemsToMatchHeaders(newValue) {
      let obj
      let headersSelected = []
      let itemsArr = []

      for (let item of newValue) { //getting the current headers of table
        headersSelected.push(item.value)
      }

      for (let item of this.items) { //getting from all items only the values which match the current headers
        obj = {}
        for (let key in item) {
          for (let index = 0; index < headersSelected.length; index++) {
            if (headersSelected[index] === key) {
              obj[key] = item[key]
            }
            obj.id = item.id
            if (item.lastName){
              obj.lastName = item.lastName
            }
          }
        }
        itemsArr.push(obj)
      }
      this.itemsToDisplay = itemsArr
    },
    deleteItem(rowID){
      this.$emit('onDelete', rowID)
    },
    addRow() {
      this.$emit('onAddRow')
    },
    shouldShow(columnPosition) {
      return this.existsPosition(columnPosition)
    },
    existsPosition(position) { //checking if the header has a position, if so output the row
      let positionFound = false
      for (let item of this.headersSelected) {
        if (item.position === position)
          positionFound = true
      }
      return positionFound
    },
    filterSessionCases () {
      const itemsArray = JSON.parse(JSON.stringify(this.items))

      let newArr = itemsArray.map( el => {
        if (el.description.length > 40) {
          el.description = el.description.substring(0, 40)
        }
        return el
      })

      this.itemsToDisplay = newArr
    },
    takeDescriptionText (id) {
      let description = ''

      for (let item in this.items) {
        if (this.items[item].id === id) {
          description = this.items[item].description
        }
      }

      return description
    },
    checkLength (id) {
      let isLongText = false
      this.items.find( el => {
        if (el.id === id) {
          if (el.description.length > 40) {
            isLongText = true
          }
        }
      })

      return isLongText
    },
    openFilter() {
      this.modal = true
    },
    getSelectedColumnsSelectionText() {
      return this.headersSelected
        .map(a => a.text)
        .slice(0, 2)
        .join(', ')
    }
  }
}
