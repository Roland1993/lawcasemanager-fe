export const computed = {
  computed: {
    sortedHeaders()
    { //sort headers to display by position  in order to have the action header at the end
      let passedHeaders = JSON.parse(JSON.stringify(this.passedSelectedHeaders))
      let headersSelected = JSON.parse(JSON.stringify(this.headersSelected))
      let newAddedHeaders = []

      if (JSON.stringify(passedHeaders) === JSON.stringify(headersSelected)) {
        return passedHeaders
      }

      for (let item of headersSelected) {
        if (!JSON.stringify(passedHeaders).includes(JSON.stringify(item))) {
          passedHeaders.push(item)
        }
      }

      passedHeaders.sort((a, b) => {
        return a.position - b.position
      })

      return passedHeaders

    },
    filterRowsPerPage()
    {
      if (this.totalItems < 25 && this.totalItems >= 10) {
        return [5, 10];
      }
      if (this.totalItems < 10 && this.totalItems > 5) {
        return [5, this.totalItems]
      }
      if (this.totalItems <= 5) {
        return [5]
      }
      return [5, 10, 25]
    },
    isLoading()
    {
      return this.$store.getters.isLoading
    }
  }
}
