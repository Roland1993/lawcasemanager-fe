export const watch = {
  watch: {
    pagination: {
      handler () {
        this.initTable()
      },
      deep: true
    },
    headersSelected(newValue) {

      this.fillItemsToMatchHeaders(newValue)

      if (this.pagination.search.trim() !== '') { //cuz the search is SS we send request to server
        this.initTable()
      }

    },
    items(){
      if (this.tableBelongsTo('sessionCase')) {
        this.filterSessionCases()
      }else {
        this.fillItemsToMatchHeaders(this.headersSelected)
      }
    },
    filterTable(newValue) {
      if (newValue) {
        this.tableFilters = newValue
        this.isTableFiltered = true
        this.initTable()
      }else {
        this.isTableFiltered = false
        this.tableFilters = null
        this.initTable()
      }
    },
    tableUpdated (newValue) {
      if (newValue) {
        this.initTable()
      }
    },
    tableLoading (newValue) {
      this.loading = newValue
    }
  }
}
