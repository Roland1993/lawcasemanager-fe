import  { computed } from './table-mixins/computed'
import { watch } from "./table-mixins/watch";
import  { methods } from "./table-mixins/methods";
import checkUser from "../../services/checkUser"
import FilterModal from '../filter-modal/FilterModal.vue'

export default {
  components: {
    FilterModal
  },
  mixins: [computed, watch, methods],
  props: [
    'allHeaders',
    'tableContent',
    'dbResponse',
    'currentPage',
    'passedSelectedHeaders',
    'filterTable',
    'tableUpdated',
    'tableLoading'
  ],
  data () {
    return {
      isTableFiltered: false,
      tableFilters: null,
      hasSearch: false,
      modal:false,
      checkUser: checkUser,
      headersSelected: this.passedSelectedHeaders,
      search: '',
      itemsToDisplay: [],
      items: [],
      totalItems:null,
      loading: false,
      searchTimeout: null,
      pagination: {
        descending: null,
        search:'',
      },
      switch1: true,
      selected: [],
    }
  },
  name: 'Table'
}
