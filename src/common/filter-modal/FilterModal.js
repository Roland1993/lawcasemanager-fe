import checkUser from '../../services/checkUser'
import { methods } from "./filter-mixins/methods";
import { mapGetters } from 'vuex'

export default {
  mixins: [methods],
  props: {
    modal: {
      default: false
    },
    filterTable: {
      default: null
    }
  },
  created() {
    this.fetchToFillFilterFields()
  },
  data () {
    return {
      loading: false,
      checkUser: checkUser,
      year: null,
      month:'',
      department: '',
      lawyer: '',
      caseResult: '',
      archived: '0',
      monthItems: [],
      departmentItems: [],
      lawyerItems: [],
      caseResultItems: [],
      archivedItems: [
        { text: 'Të Arkivuara', value: 'Po' },
        { text: 'Jo të Arkivuara', value: 'Jo' },
        { text: 'Të gjitha', value: '0' },
      ]
    }
  },
  computed: {
    ...mapGetters([
      'getMonths',
      'getCaseResults',
      'getDepartments'
    ])
  }
}
