import makeRequestTo from '../../../services/makeRequestTo'
import checkUser from '@/services/checkUser'

export const methods = {
  methods: {
    closeModal () {
      this.$emit('update:modal', false)
      this.month = ''
      this.department = ''
      this.year = null
      this.lawyer = ''
      this.caseResult = ''
      this.archived = '0'
      this.$router.replace({name: 'Homepage'})
    },
    save () {
      if (!this.checkFilters())
      {
        this.$emit('update:modal', false)
        this.$router.replace({name: 'Homepage'})
      }else {
        const query = {
          month: this.month,
          year: this.year,
          department: this.department,
          lawyer: this.lawyer,
          caseResult: this.caseResult,
          archived: this.archived
        }
        this.$emit('update:modal', false)
        this.$router.replace({name: 'Homepage',
          query
        })
      }
    },
    clearFilters () {
      this.lawyer = ''
      this.caseResult = ''
      this.archived = '0'
      this.month = ''
      this.year = null
      this.department = null
    },
    checkFilters () {
      if (!this.lawyer && !this.caseResult && this.archived === '0' && !this.month && !this.year && !this.department)
      {
        return false
      }
      return true
    },
    async fetchToFillFilterFields () {
      await this.makeRequests()
      this.fillFilters()
    },
    makeRequests () {
      this.monthItems = this.getMonths
      this.departmentItems = this.getDepartments
      this.caseResultItems = this.getCaseResults
      if (!checkUser.isBasicUser()) return this.getLawyerFilter()
    },
    getLawyerFilter () {
      this.loading = true

      return makeRequestTo.getLawyers()
              .then(response => {
                this.loading = false
                this.lawyerItems = response.data.lawyers
              })
    },
    fillFilters () {
      if (this.filterTable) {
        this.month = this.filterTable.month
        this.department = this.filterTable.department
        this.year = this.filterTable.year
        this.lawyer = this.filterTable.lawyer
        this.caseResult = this.filterTable.caseResult
        this.archived = this.filterTable.archived
      }
    }
  }
}
