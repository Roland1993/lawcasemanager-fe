export const data = {
  data: () => ({
    tableLoading: false,
    addDialog: false,
    editDialog: false,
    tooltipText: 'Jeni duke fshirë një përdorues.Kini kujdes për arsye se ky veprim nuk mund të zbëhet.Nqs vërtet dëshironi ta fshini këtë përdorues atëhere shkruani më poshtë tekstin: jam i sigurte',
    btnText: 'Fshi cështjen',
    deleteDialog: false,
    userIDtoDelete: null,
    tableUpdated: false,
    rowID: null,
    drawer:false,
    search: '',
    tableBelongsTo: 'user',
    headers: [
      { text: 'Emër Mbiemër', value: 'nameSurname', align: 'center', position: 1, align: 'center' },
      { text: 'E-mail', value: 'email', position: 2, align: 'center' },
      { text: 'Departamenti', value: 'departmentText', position: 3, align: 'center' },
      { text: 'Tipi i Përdoruesit', value: 'userTypeText', position: 4, align: 'center' },
      { text: 'Statusi', value: 'statusText', position: 5, align: 'center' },
      { text: 'Veprime', value: 'name', sortable: false, align: 'center', position: 14 }
    ],
    tableContent: 'getUsers',
    dbResponse: 'response.data.users',
    items: [],
    rowsPerPage: [5,10,25],
    loading: false,
    pagination: {},
    totalItems: 0,
    passedEditedRow: null
  }),
}
