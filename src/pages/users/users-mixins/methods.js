import makeRequestTo from "../../../services/makeRequestTo";

export const methods = {
  methods: {
    editItem (row) {
      this.editDialog = true
      this.passedEditedRow = row
    },
    addRow () {
      this.addDialog  = true
    },
    updateTable () {
      this.tableUpdated = true
    },
    confirmDelete (id) {
      this.userIDtoDelete = id
      this.deleteDialog = true
    },
    async deleteRow() {
      this.deleteDialog = false
      const id = {id: this.userIDtoDelete}

      this.tableLoading = true
      await this.deleteUser(id)
      this.tableLoading = false
      this.tableUpdated = true
    },
    deleteUser (id) {
      return makeRequestTo.deleteUser(id)
        .then(response => {
          this.openSuccessSnackbar(response.data.message)
        })
    },
    tableFinishUpdate () {
      this.tableUpdated = false
    },
  }
}
