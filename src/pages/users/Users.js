import EditDialog from '../../components/edit-user/EditUser.vue'
import AddDialog from '../../components/add-user/AddUser.vue'
import Table from '../../common/table/Table.vue'
import { data } from "./users-mixins/data";
import { methods } from "./users-mixins/methods";
import DeleteDialog from '../../components/delete-dialog/DeleteDialog.vue'
import { snackbar } from "../../mixins/snackbar";

export default {
  mixins: [data, methods, snackbar],
  components: {
    EditDialog,
    AddDialog,
    Table,
    DeleteDialog
  },
}
