import { methods } from "./home-mixins/methods"
import { data } from './home-mixins/data'
import Table from '../../common/table/Table.vue'
import EditCase from '@/components/edit-case/EditCase.vue'
import DeleteCase from '@/components/delete-dialog/DeleteDialog.vue'
import { eventBus } from "../../main"
import { snackbar } from "../../mixins/snackbar";
import AddCase from '@/components/add-case/AddCase.vue'

export default {
  mixins: [data, methods, snackbar],
  components: {
    Table,
    EditCase,
    DeleteCase,
    AddCase
  },
  created () {
    eventBus.$on('caseAdded', () => {
      this.updateTable()
    })
  },
  watch: {
    '$route.query'(queryParams) {
      if(queryParams.lawyer || queryParams.caseResult || queryParams.archived || queryParams.month || queryParams.year || queryParams.department) {
        this.filterTable = {
          month: queryParams.month,
          year: queryParams.year,
          department: queryParams.department,
          lawyer: queryParams.lawyer,
          caseResult: queryParams.caseResult,
          archived: queryParams.archived
        }
      }else {
        this.filterTable = null
      }
    }
  },
  beforeRouteEnter(to, from, next) {
    if(Object.keys(to.query).length !== 0) {
      if(to.query.lawyer || to.query.caseResult || to.query.archived || to.query.month || to.query.year || to.query.department) {
        next(vm => {
          vm.filterTable =  {
            month: to.query.month,
            year: to.query.year,
            department: to.query.department,
            lawyer: to.query.lawyer,
            caseResult: to.query.caseResult,
            archived: to.query.archived
          }
        })
      }
    }
    next()
  },

}
