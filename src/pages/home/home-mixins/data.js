import checkUser from '@/services/checkUser'

export const data = {
  data() {
    return {
      addDialog: false,
      checkUser: checkUser,
      tableLoading: false,
      tableUpdated: false,
      editDialog: false,
      tooltipText: 'Jeni duke fshirë një cështje.Kini kujdes për arsye se ky veprim nuk mund të zbëhet.Nqs vërtet dëshironi ta fshini këtë cështje atëhere shkruani më poshtë tekstin: jam i sigurte',
      btnText: 'Fshi cështjen',
      deleteDialog: false,
      caseIDtoDelete: null,
      tableRow: null,
      filterTable: null,
      dbResponse: 'response.data.cases',
      tableContent: 'getCases',
      tableBelongsTo: 'case',
      allHeaders: [
        { text: 'Nr. Çështjes', value: 'caseNumber', selected: true, disabled: true, position: 1, align: 'center' },
        { text: 'Rez. Çështjes', value: 'caseResult', selected: true, disabled: true, position: 2, align: 'center' },
        { text: 'Nr. Aktit', value: 'nrakti', selected: true, disabled: true, position: 3, align: 'center' },
        { text: 'Muaji', value: 'monthText', selected: true, disabled: true, position: 4, align: 'center' },
        { text: 'Paditësi', value: 'plaintiff', selected: true, disabled: true, position: 5, align: 'center' },
        { text: 'I padituri', value: 'defendent', position: 6, align: 'center' },
        { text: 'Palët e Treta', value: 'thirdParties', position: 7, align: 'center' },
        { text: 'Objekti', value: 'object', position: 8, align: 'center' },
        { text: 'Vlera e Padisë (mln lek)', value: 'claimValue', position: 9, align: 'center' },
        { text: 'Gjykatësi', value: 'judge', position: 10, align: 'center' },
        { text: 'Gjykata', value: 'courtText', position: 11, align: 'center' },
        { text: 'Avokati', value: 'nameSurname', position: 12, align: 'center' },
        { text: 'Arkivuar', value: 'archived', position: 13, align: 'center' },
        { text: 'Veprime', sortable: false, value: 'veprime', persist: true, selected: true, disabled: true, align: 'center', position: 14 }
      ],
    }
  },
  computed: {
    selectedHeaders () {
      let selectedHeaders = [
        { text: 'Nr. Çështjes', value: 'caseNumber', align: 'center', selected: true, disabled: true, position: 1 },
        { text: 'Rez. Çështjes', value: 'caseResult', align: 'center', selected: true, disabled: true, position: 2 },
        { text: 'Nr. Aktit', value: 'nrakti', align: 'center', selected: true, disabled: true, position: 3 },
        { text: 'Muaji', value: 'monthText', align: 'center', selected: true, disabled: true, position: 4 },
        { text: 'Paditësi', value: 'plaintiff', align: 'center', selected: true, disabled: true, position: 5 },
      ]

      if (checkUser.managePermissions('case', 'edit') || checkUser.managePermissions('case', 'delete')) {
        const actions = { text: 'Veprime', sortable: false, value: 'veprime', persist: true, selected: true, disabled: true, align: 'center', position: 14 }
        selectedHeaders.push(actions)
      }

      return selectedHeaders

    }
  }
}
