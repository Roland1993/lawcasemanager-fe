import makeRequestTo from '@/services/makeRequestTo'
import axios from 'axios'

export const methods = {
  methods: {
    addRow() {
      this.addDialog = true
    },
    editRow(row) {
      this.tableRow = row
      this.editDialog = true
    },
    async deleteRow() {
        this.deleteDialog = false
        const id = {id: this.caseIDtoDelete}

        this.tableLoading = true
        await this.makeDeleteRequests(id)
        this.tableLoading = false
        this.tableUpdated = true
    },
    confirmDelete(rowID) {
      this.caseIDtoDelete = rowID
      this.deleteDialog = true
    },
    makeDeleteRequests (id) {
      return axios.all([
        makeRequestTo.deleteCase(id),
        makeRequestTo.deteleCaseResult(id)
      ])
        .then(axios.spread((caseResponse, caseResultResponse) => {
          this.openSuccessSnackbar('Cështja u fshi me sukses')
        }))
    },
    updateTable () {
      this.tableUpdated = true
    },
    tableFinishUpdate () {
      this.tableUpdated = false
    }
  }
}
