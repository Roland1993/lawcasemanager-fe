import makeRequestTo from '../../services/makeRequestTo'
import { required, email } from 'vuelidate/lib/validators'
import { mapGetters } from 'vuex'

export default {
  validations: {
    email: { required, email },
    password: { required }
  },
  data () {
    return {
      email: '',
      password: '',
      error: false,
      errorMessage: '',
      loading: false
    }
  },
  computed: {
    isLoading() {
      return this.$store.getters.isLoading
    }
  },
  methods: {
    login () {

      if (this.email.trim() && this.password.trim() && !this.$v.$error) {
        const obj = {
          email: this.email,
          password: this.password
        }
        this.error = false
        this.loading = true
        makeRequestTo.login(obj)
          .then( res => {
            this.loading = false
            this.$store.commit('setToken', res.data.token)
            this.$store.commit('changeIsLogged',true)
            this.$store.commit('setUserLogged',res.data.user)
            this.$router.push({name: 'Homepage'})
          })
          .catch(err => {
            this.loading = false
            this.error = true
            this.errorMessage = err.response.data.message
          })
      }else {
        this.$v.$touch()
      }
    }
  },
  name: 'Login'
}
