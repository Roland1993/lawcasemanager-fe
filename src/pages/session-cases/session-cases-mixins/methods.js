import makeRequestTo from "../../../services/makeRequestTo";

export const methods = {
  methods: {
    addRow() {
      this.addDialog = true
    },
    editRow (row) {
      this.editDialog = true
      this.sessionCaseDataToEdit = row
    },
    confirmDelete(rowID) {
      this.sessionCaseIDtoDelete = rowID
      this.deleteDialog = true
    },
    async deleteRow() {
      this.deleteDialog = false
      const id = { id: this.sessionCaseIDtoDelete }
      this.tableLoading = true
      await this.makeDeleteRequests(id)
      this.tableLoading = false
      this.tableUpdated = true
    },
    makeDeleteRequests (id) {
      return makeRequestTo.deleteSessionCase(id)
        .then(response => {
          this.openSuccessSnackbar(response.data.message)
        })
    },
    updateTable () {
      this.tableUpdated = true
    },
    tableFinishUpdate () {
      this.tableUpdated = false
    }
  }
}
