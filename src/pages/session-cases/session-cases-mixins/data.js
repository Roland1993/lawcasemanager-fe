import checkUser from '@/services/checkUser'

export const data = {
  data () {
    return {
      checkUser: checkUser,
      tableLoading: false,
      addDialog: false,
      editDialog: false,
      tooltipText: 'Jeni duke fshirë një seancë.Kini kujdes për arsye se ky veprim nuk mund të zbëhet.Nqs vërtet dëshironi ta fshini këtë seancë atëhere shkruani më poshtë tekstin: jam i sigurte',
      btnText: 'Fshi Seancë',
      deleteDialog: false,
      sessionCaseIDtoDelete: null,
      sessionCaseDataToEdit: null,
      tableUpdated: false,
      drawer:false,
      search: '',
      tableBelongsTo: 'sessionCase',
      tableContent: 'get/session/case',
      dbResponse: 'response.data.sessionCases',
      items: [],
      rowsPerPage: [5,10,25],
      loading: false,
      pagination: {},
      totalItems: 0,
    }
  },
  computed: {
    headers () {
      let headers = [
        { text: 'Numri i Cështjes', value: 'caseNumber', align: 'center', position: 1, align: 'center' },
        { text: 'Përshkrimi', value: 'description', position: 2, align: 'center' },
      ]

      if (checkUser.managePermissions('sessionCase', 'edit') || checkUser.managePermissions('sessionCase', 'delete')) {
        const actions = { text: 'Veprime', value: 'name', sortable: false, align: 'center', position: 14 }
        headers.push(actions)
      }

      return headers
    }
  }
}
