import Table from '@/common/table/Table.vue'
import AddDialog from '@/components/add-session-case/AddSessionCase.vue'
import EditDialog from '@/components/edit-session-case/EditSessionCase.vue';
import DeleteDialog from '@/components/delete-dialog/DeleteDialog.vue'
import { methods } from "./session-cases-mixins/methods";
import { data } from "./session-cases-mixins/data";
import { snackbar } from "../../mixins/snackbar";


export default {
  mixins: [data, methods, snackbar],
  components: {
    Table,
    EditDialog,
    DeleteDialog,
    AddDialog
  },
}
